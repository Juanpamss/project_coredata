//
//  ContactTableViewCell.swift
//  Proyect_CoreData
//
//  Created by Juan Pa on 30/1/18.
//  Copyright © 2018 Juan Pa. All rights reserved.
//

import UIKit

class ContactTableViewCell: UITableViewCell {

    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func fillData(persona:Persona){
        
        nameLabel.text = persona.name
        addressLabel.text = persona.address
        phoneLabel.text = persona.phone
    }
    

}
