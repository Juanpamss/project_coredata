//
//  ViewController.swift
//  Proyect_CoreData
//
//  Created by Juan Pa on 23/1/18.
//  Copyright © 2018 Juan Pa. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {

    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelAddress: UILabel!
    @IBOutlet weak var labelPhone: UILabel!
    @IBOutlet weak var textName: UITextField!
    @IBOutlet weak var textAddress: UITextField!
    @IBOutlet weak var textPhone: UITextField!
    
    let manageObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var singlePerson:Persona?
    var allPeople:[Persona] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func savePerson (){
        
        let entityDescription = NSEntityDescription.entity(forEntityName: "Persona", in: manageObjectContext)
        
        let person = Persona(entity: entityDescription!, insertInto: manageObjectContext)
        
        person.address = textAddress.text ?? ""
        person.name = textName.text ?? ""
        person.phone = textPhone.text ?? ""
        
        do{
            
            try manageObjectContext.save()
            
            clearFields()
            
        } catch {
            
            print("Error")
            
        }
        
    }
    
    func clearFields(){
        
        textAddress.text = ""
        textPhone.text = ""
        textName.text = ""
        
    }

    @IBAction func saveButtonPressed(_ sender: Any) {
        
        savePerson()
        
        
    }
    
    @IBAction func findButtonPressed(_ sender: Any) {
        
        if(textName.text == ""){
            findAll()
        }else{
            
            findOne()
        }
        
    }
    
    func findAll(){
        
        let request:NSFetchRequest<Persona> = Persona.fetchRequest()
        
        do{
            let results = try( manageObjectContext.fetch(request as! NSFetchRequest<NSFetchRequestResult> ))
            
            for result in results {
                
                let person = result as! Persona
                
                allPeople.append(person)
                
            }
            
            try manageObjectContext.save()
            
            clearFields()
            
            performSegue(withIdentifier: "DeleteViewController", sender: self)
            
        } catch {
            
            print("Error")
            
        }
        
    }
    
    func findOne(){
        
        let entityDescription = NSEntityDescription.entity(forEntityName: "Persona", in: manageObjectContext)
        
        let request:NSFetchRequest<Persona> = Persona.fetchRequest()
        
        request.entity = entityDescription
        
        let predicate = NSPredicate(format: "name = %@", textName.text!)
        
        request.predicate = predicate
        
        do{
            let results = try( manageObjectContext.fetch(request as! NSFetchRequest<NSFetchRequestResult> ))
            
            if results.count > 0{
                
                let match = results[0] as! Persona
                
                singlePerson = match
            
                performSegue(withIdentifier: "DeleteViewController", sender: self)
                
                
                
                
            }else {
                
                textAddress.text = "n/a"
                textPhone.text = "n/a"
                textName.text = "n/a"
                
            }
            
        }catch{
            
            print("Error finding one")
            
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "DeleteViewController"{
            
            let destination = segue.destination as! DeleteViewController
            
            destination.allPeople = allPeople
            
            
        }
    }
    
}

