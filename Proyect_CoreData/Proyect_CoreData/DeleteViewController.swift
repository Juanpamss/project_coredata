//
//  DeleteViewController.swift
//  Proyect_CoreData
//
//  Created by Juan Pa on 30/1/18.
//  Copyright © 2018 Juan Pa. All rights reserved.
//

import UIKit
import CoreData

class DeleteViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{

    
    @IBOutlet weak var contactTableView: UITableView!
    
    var allPeople:[Persona] = []
    var index = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    func numberOfSections(in tableView: UITableView) -> Int { //Número de secciones
        return 1
    }
    
    func get20FistPokemons(person: [Persona]) {
        allPeople = person
        contactTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { //Número de 
        return allPeople.count
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath?{
        index = indexPath.row
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell { //Devuelve la información de cada fila
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "contactCustomCell") as! ContactTableViewCell
        
        cell.fillData(persona: allPeople[indexPath.row])
        
        return cell
    }

    
    
//    @IBAction func deleteButtonPressed(_ sender: Any) {
//
//        let manageObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
//
//        manageObjectContext.delete(person!)
//
//        do{
//            try manageObjectContext.save()
//
//        }catch{
//
//            print("Error al borrar")
//        }
//
//    }
    

    

}
